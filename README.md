# ripDVDs

## Requirements
- Don Melton's transcoding scripts. (gem install video_transcoding) See: `https://github.com/donmelton/video_transcoding`
- HandBrakeCLI, libdvdcss. I used `brew install handbrake ffmpeg libdvdcss mkvtoolnix mp4v2`

## Modify script
- Modify the value of $OUTDIR near top of script to the output directory into which you want the generated .m4v files placed after encoding is finished. If set to empty string or undef,
the file will not be moved.
- Install the script ripDVDs somewhere or execute it from the git repo

## Use
- Insert first DVD
- Run this script
- Once first DVD is ejected, insert the next, and repeat

## Overview of process
1. The script looks for a file /Volumes/\*/VIDEO_TS file and if found  assumes it is
a Video DVD, if not, sleeps for 10 seconds and repeats this step
1. .Look for a previous version of the same .m4v file and removes it
1. Rips DVD to a .m4v file using the command `transcode-video --quick --add-subtitle all --add-audio all --m4v` (adjust variable $make_m4v if you want other options)
1. If $OUTPUT not null, move .m4v to $OUTPUT directory
1. eject DVD
1. back to step 1
